# week1

## Lesson November 14, 2022 9am-11am

1. Module 2 introduction: [pdf](./slides/Machine_Learning_Systems_For_Data_Science.pdf)
2. Jupyter Notebook: [pdf](./slides/Machine_Learning_Systems_For_Data_Science_Notebook.pdf)
 - Markdown cell: [notebook](./scripts/Markdown_cell.ipynb)
3. Python Libraries: [pdf](./slides/Machine_Learning_Systems_For_Data_Science_Libraries.pdf)

## Lesson November 14, 2022 4pm-6pm
1. Markdown cell: [notebook](./scripts/Markdown_cell.ipynb)
 - Markdown exercises: [notebook](./exercises/Markdown_exercises.ipynb)
2. Check python libraries' versions: [notebook](./scripts/check_python_modules.ipynb)
3. Setup script: [notebook](./scripts/setup.ipynb)
4. Numpy Library: [notebook](./scripts/numpy_library.ipynb)
 - Numpy exercises: [notebook](./exercises/Numpy_exercises.ipynb)
5. Pandas Library
 - Series pandas: [notebook](./scripts/series_pandas.ipynb)
 - Series exercises: [notebook](./exercises/Series_pandas_exercises.ipynb)
